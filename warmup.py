import requests
from tqdm import tqdm

def warmup_emails_simplified():
    # Prompt the user for the API key
    api_key = input("Please enter your API key: ")
    
    # Define the API endpoint to get the IDs
    base_url = "https://server.smartlead.ai/api/v1/email-accounts/"
    params = {
        "api_key": api_key,
        "offset": 0,
        "limit": 100  # Fetching in chunks of 100, can be adjusted if needed
    }
    
    all_ids = []

    # Fetching all IDs using pagination
    while True:
        response = requests.get(base_url, params=params)
        response.raise_for_status()  # Raise an exception for HTTP errors
        data = response.json()
        
        # Break the loop if no more data is returned
        if not data:
            break
        
        # Extract the IDs from the current chunk and add to the main list
        ids_chunk = [item['id'] for item in data]
        all_ids.extend(ids_chunk)
        
        # If the returned data is less than the limit, it means we've reached the end
        if len(data) < params["limit"]:
            break
        
        # Increase the offset for the next loop iteration
        params["offset"] += params["limit"]
    
    # Define the API endpoint and headers for the warmup process
    url_template = f"https://server.smartlead.ai/api/v1/email-accounts/{{}}/warmup?api_key={api_key}"
    headers = {
        "Content-Type": "application/json"
    }
    
    # Data payload for the request
    payload = {
        "warmup_enabled": True, 
        "total_warmup_per_day": 50,
        "daily_rampup": 50,
        "reply_rate_percentage": 100,
        "warmup_key_id": "YOUR-TAGS"
    }

    successful_updates = 0

    # Loop through each ID, using tqdm for the progress bar
    for id_ in tqdm(all_ids, desc="Updating IDs", unit="ID"):
        response = requests.post(url_template.format(id_), headers=headers, json=payload)
        if response.status_code == 200:  # Assuming 200 is the success status code
            successful_updates += 1

    print(f"\nTotal successful updates: {successful_updates}/{len(all_ids)}")
    print("All requests completed!")

# Directly calling the function to ensure it runs immediately upon script execution
warmup_emails_simplified()
