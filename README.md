![](https://i.imgur.com/w3COuyF.jpg)

# Smartlead Bulk Warmup Setup Script

### Purpose

This script will make an API call to Smartlead to bulk update all accounts (Superwave or not) with the Superwave manual's parameters for warmup at the press of a button.

### Usage

After downloading the script you need to fetch your Smartlead API key from [your profile menu](https://app.smartlead.ai/app/settings/profile).

You should modify the `warmup_key_id` section in the code with your actual identifier tags used in Smartlead.

Run `python warmup.py` in your terminal as needed (Note: You are required to have Python and the required libraries installed).

You can also set the script to run every 24 hours as a `cron` job for any reason whatsoever (Such as adding new accounts).

### Warning

If you don't know what you're doing have someone who knows do it for you. You're solely responsible for your own actions.

This will update all the email addresses in your Smartlead account!

